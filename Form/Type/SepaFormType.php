<?php

namespace Novalnet\Bundle\NovalnetBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Novalnet\Bundle\NovalnetBundle\PaymentMethod\NovalnetHelper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

/**
 * {@inheritdoc}
 */
class SepaFormType extends AbstractType
{
    const BLOCK_PREFIX = 'novalnet_sepa_form_type';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('sepaSavedAccountDetails', SepaSavedAccountDetailsType::class, [
            'label' => false,
            'masked_data' => $options['masked_data']
        ]);
        $builder
            ->add(
                'sepaiban',
                TextType::class,
                [
                    'label' => 'novalnet.sepa.iban.form.label',
                    'required' => true,
                    'attr' => [
                        'autocomplete' => 'off',
                    ],
                    'constraints' => [
                        new NotBlank(),
                    ],
                ]
            )
            ->add(
                'sepaSaveAccountDetails',
                CheckboxType::class,
                [
                    'label' => 'I want to save my account details for later purchases',
                    'attr' => ['checked' => true]
                ]
            );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'masked_data' => [],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return self::BLOCK_PREFIX;
    }
}
